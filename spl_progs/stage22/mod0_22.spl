// Resoure Manager Module (MOD_0)
// called from mod4 with either acquire terminal (8) or release terminal (9)

alias functionNum R1;                                        // contains function identifier

if (functionNum == 3) then												// acquire disk

	alias currentPID R2;                                         // current process PID
	if ([DISK_STATUS_TABLE + 0] == 1) then                 // disk is busy
		[PROCESS_TABLE + R2 * 16 + 4] = WAIT_DISK;
		multipush(R1, R2);
		call MOD_5;
		multipop(R1, R2);
	endif;
	[DISK_STATUS_TABLE + 0] = 1;            // locked
	[DISK_STATUS_TABLE + 4] = R2;				// PID
	return;
endif;

if (functionNum == 6) then												// Acquire semaphore
	alias pid R2;
	alias sem_id R3;
	sem_id = 0;
	while (sem_id < MAX_SEM_COUNT) do
		if ([SEMAPHORE_TABLE + sem_id*4 + 1] == 0) then
			[SEMAPHORE_TABLE + sem_id*4 + 0] = -1;					// locking pid
			[SEMAPHORE_TABLE + sem_id*4 + 1] = 1;					// proc count
			R0 = sem_id;
			return;
		endif;
		sem_id = sem_id + 1;
	endwhile;
	R0 = -1;
	return;
endif;

if (functionNum == 7) then												// Release semaphore
	alias sem_id R2;
	alias pid R3;
	
	if (pid < 0 || pid > 15) then
		R0 = -1;
		return;
	endif;
	if (sem_id < 0 || sem_id >= MAX_SEM_COUNT) then
		R0 = -1;
		return;
	endif;
	if ([SEMAPHORE_TABLE + sem_id*4 + 0] == pid) then
		[SEMAPHORE_TABLE + sem_id*4 + 0] = -1;
		[SEMAPHORE_TABLE + sem_id*4 + 1] = [SEMAPHORE_TABLE + sem_id*4 + 1] - 1;

		alias idx R4;
		idx = 0;
		while (idx < MAX_PROC_NUM) do
			if ([PROCESS_TABLE + idx*16 + 4] == WAIT_SEMAPHORE && [PROCESS_TABLE + idx*16 + 5] == sem_id) then
				[PROCESS_TABLE + idx*16 + 4] = READY;
			endif;
			idx = idx + 1;
		endwhile;
	endif;
	R0 = 0;
	return;
endif;

if (functionNum == 8) then
                                                             // acquire terminal

 alias currentPID R2;                                         // current process PID
 if ([TERMINAL_STATUS_TABLE + 0] == 1) then
  [PROCESS_TABLE + currentPID * 16 + 4] = WAIT_TERMINAL; // change the current state as waiting for terminal

                                                             // since terminal isn't free yet, we can not proceed further and should schedule another
                                                             // process which might be using terminal, so that terminal could be released and used
                                                             // timer interrupts are disabled in kernel mode, so only way to schedule a new process
                                                             // is by calling the module 5 (process scheduler) from here itself (module can call other modules)

  multipush(R1,R2);                                      // store context as this is voluntary call of other module

  call MOD_5;                                            // module 5 - process scheduler

  multipop(R1,R2);                                       // restore the context
 endif;

                                                             // control reaches here when terminal gets free and can be acquired by the current process
                                                             // now terminal status will be 0 since its free to use

 [TERMINAL_STATUS_TABLE + 0] = 1;                          // now set the terminal state to 1 to block it
 [TERMINAL_STATUS_TABLE + 1] = currentPID;                 // set the current process' PID
 return;
endif;

if (functionNum == 9) then
                                                             // release terminal

 alias currentPID R2;                                         // current process PID
 alias retValue R0;
 if (currentPID != [TERMINAL_STATUS_TABLE + 1]) then
  retValue = -1;                                         // error - trying to release the terminal without acquiring
  return;
 endif;
 // breakpoint;
 [TERMINAL_STATUS_TABLE + 0] = 0;                          // set released
	// breakpoint;
 [PROCESS_TABLE + currentPID * 16 + 4] = READY;            // set the process to be ready
 R0 = 0;                                                   // return value is zero
 return;
endif;
